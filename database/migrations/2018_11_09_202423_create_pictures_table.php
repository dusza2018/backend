<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(
            'pictures',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('artist_id');
                $table->unsignedInteger('category_id');
                $table->integer('visited')->default(0);
                $table->integer('liked')->default(0);
                $table->integer('reviewed')->default(0);
                $table->string('filepath', 255);
                $table->string('title')->nullable();
                $table->text('description')->nullable();
                $table->dateTime('refused_at')->nullable();
                $table->rememberToken();
                $table->timestamps();

                $table->foreign('artist_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

                $table->foreign('category_id')
                    ->references('id')
                    ->on('categories')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pictures');
    }
}
