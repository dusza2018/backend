<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevisionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(
            'revisions',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('mentor_id');
                $table->unsignedInteger('picture_id');
                $table->integer('value')->default(0);
                $table->text('description')->nullable();
                $table->string('file_path')->nullable();
                $table->timestamps();

                $table->foreign('mentor_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

                $table->foreign('picture_id')->references('id')->on('pictures')->onUpdate('cascade')->onDelete(
                        'cascade'
                    );
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('revisions');
    }
}
