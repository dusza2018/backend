<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'artist_id',
        'category_id',
        'filepath',
        'title',
        'visited',
        'liked',
        'reviewed',
        'description',
        'refused_at',
    ];

    public function artist() {
        return $this->belongsTo(User::class, 'artist_id');
    }

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function revisions() {
        return $this->hasMany(Revision::class)->with('user');
    }
}
