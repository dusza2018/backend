<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'picture_id', 'ip'
    ];
}
