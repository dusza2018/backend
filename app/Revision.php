<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mentor_id',
        'value',
        'description',
        'picture_id',
        'file_path',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'mentor_id');
    }
}
