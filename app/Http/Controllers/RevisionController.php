<?php

namespace App\Http\Controllers;

use App\Revision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RevisionController extends Controller {
    public function index() {
        $revisions = Revision::all();

        return $revisions;
    }

    public function store(Request $request) {
        $revision = Revision::create($request->all());
        Log::info("Kategória hozzáadva: {$revision->name}");

        return response($revision);
    }

    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();
            $revision = Revision::findOrFail($id);
            $revision->fill($request->all());
            $revision->save();
            Log::info("{$revision->name} értékelés sikeresen módosítva.");
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A értékelés módosítása sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($revision);
    }

    public function show($id) {
        $revision = Revision::findOrFail($id);

        return response($revision);
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            $revision = Revision::findOrFail($id);
            $revision->delete();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A értékelés törlése sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($revision);
    }

    public function getRevisionsByMentor() {
        return Revision::with('user')->select('mentor_id', DB::raw('count(*) as total'))->groupBy('mentor_id')->get();
    }
}
