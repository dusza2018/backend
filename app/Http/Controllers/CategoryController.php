<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller {
    public function index() {
        $categorys = Category::all();

        return $categorys;
    }

    public function store(Request $request) {
        $category = Category::create($request->all());
        Log::info("Kategória hozzáadva: {$category->name}");

        return response($category);
    }

    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();
            $category = Category::findOrFail($id);
            $category->fill($request->all());
            $category->save();
            Log::info("{$category->name} kategória sikeresen módosítva.");
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A kategória módosítása sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($category);
    }

    public function show($id) {
        $category = Category::findOrFail($id);

        return response($category);
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            $category = Category::findOrFail($id);
            $category->delete();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A kategória törlése sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($category);
    }
}
