<?php

namespace App\Http\Controllers;

use App\Advertisement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdvertisementController extends Controller {
    public function index() {
        $ads = Advertisement::all();

        return $ads;
    }

    public function getRandom() {
        return Advertisement::all()->random(1);
    }

    public function stepClicks($id) {
        $ad = Advertisement::findOrFail($id);
        $ad->clicked++;
        if (!$ad->save()) {
            abort(500, "Nem sikerült a mentés");
        };

        return [];
    }

    public function store(Request $request) {
        $ad = Advertisement::create($request->all());
        Log::info("ad hozzáadva: {$ad->name}");

        return response($ad);
    }

    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();
            $ad = Advertisement::findOrFail($id);
            $ad->fill($request->all());
            $ad->save();
            Log::info("{$ad->name} hirdetés sikeresen módosítva.");
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A hirdetés módosítása sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($ad);
    }

    public function show($id) {
        $ad = Advertisement::findOrFail($id);

        return response($ad);
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            $ad = Advertisement::findOrFail($id);
            $ad->delete();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A hirdetés törlése sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($ad);
    }
}
