<?php

namespace App\Http\Controllers\files;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UploadHelper {

    private $folder;
    private $filenames;
    private $files;

    public function __construct($files, $containFolder = "other_files") {
        $this->folder = $containFolder;
        if (!Storage::exists($containFolder)) {
            Storage::makeDirectory($containFolder);
        }
        $this->files = $files;
        $this->filenames = array();
    }

    public function saveFiles() {
        foreach ($this->files as $file) {

            $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $newFilename = $this->getFileName($filename, $extension);

            $file->storeAs($this->folder, $newFilename);
            array_push($this->filenames, ["file_path" => $this->getFullPath($newFilename)]);
        }

        return $this->filenames;

    }

    public function getFullPath($filename) {
        $path = "{$this->folder}/{$filename}";
        if (!Storage::exists($path) || !Storage::size($path)) {
            return null;
        };

        return $path;
    }

    public static function deleteFiles($fileDatas) {
        foreach ($fileDatas as $file) {

            if (Storage::exists($file["file_path"])) {
                Storage::delete($file["file_path"]);
            }
        }
    }

    private function getFileName($originalName, $extension) {
        $fullName = "{$originalName}.{$extension}";
        if (!$this->checkAlreadyExists($fullName)) {
            return $fullName;
        }

        return $this->changeFilename($originalName, $extension);

    }

    private function changeFilename($originalName, $extension) {
        $appendNum = 0;
        do {
            $appendNum++;
            $newName = "{$originalName}({$appendNum}).{$extension}";

        } while ($this->checkAlreadyExists($newName));

        return $newName;

    }

    private function checkAlreadyExists($filename) {
        $exists = Storage::exists("{$this->folder}/{$filename}");

        return $exists;
    }

}
