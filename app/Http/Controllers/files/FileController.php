<?php

namespace App\Http\Controllers\files;


use App\Http\Controllers\Controller;
use App\temp_files;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller {

    public function store(Request $request) {
        if (!$request->hasFile('newFile')) {
            Log::warning("UploadController@store Did not receive the attachment file!");
            $this->response->error('Nem sikerült a fájl mentése a szerverre!', Response::HTTP_BAD_REQUEST);
        }

        $files = $request->file('newFile');
        $uploader_id = $request->get('uploader_id');
        $uploader = new UploadHelper($files, $uploader_id ?: 'tmp_files');
        $fileDatas = $uploader->saveFiles();
        $savedData = [];
        foreach ($fileDatas as $fileData) {
            //save records to DB
            $fileData['uploader_id'] = $uploader_id;
            $newData = $this->addFileToDB($fileData);
            array_push($savedData, $newData);
        }

        return $savedData;
    }

    private function addFileToDB($data) {

        $fileClass = new temp_files();
        $fileClass->fill($data);
        if (!$fileClass->save() || !$data) {
            $this->response->error('could_not_save_file_to_database', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $fileClass->toArray();
    }


    public function destroy($ids) {
        $idsArray = explode(",", $ids);
        //delete files physically
        Log::info("delete files" . json_encode($idsArray));
        UploadHelper::deleteFiles(temp_files::whereIn('id', $idsArray)->get());

        return [temp_files::whereIn('id', $idsArray)->delete()];
    }

    public function getFile($fileName) {
        if (!Storage::exists("attachments/{$fileName}")) {
            Log::warning("UploadController@getFile The requested file could not be found ({$fileName})");
            $this->response->error('A kért fájl nem található!', Response::HTTP_BAD_REQUEST);
        }

        return ["url" => Storage::url("attachments/{$fileName}")];
    }
}
