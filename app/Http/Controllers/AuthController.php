<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignUpRequest;
use App\User;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller {

    public function signUp(SignUpRequest $request) {
        $user = new User($request->all());
        if (!$user->save()) {
            return response()->json(['error' => 'Could not create user'], 500);
        }

        return response()->json(['status' => 'ok']);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request) {
        $credentials = [
            "password" => $request->get('password'),
            "username" => $request->get('username')];

        $token = auth()->attempt($credentials);

        if (!$token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        Log::info('Login with: ' . $credentials['username']);

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me() {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token) {
        return response()->json(
            [
                'user' => auth()->user(),
                'token'      => $token,
                'expires_in' => auth()->factory()->getTTL() * 60,
            ]
        );
    }
}