<?php

namespace App\Http\Controllers;

use App\Picture;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PictureController extends Controller {
    public function index() {
        $pics = Picture::with('artist')->get();

        return response($pics);
    }

    public function store(Request $request) {

        $picsNum = Picture::where('artist_id', '=', $request->get('artist_id'))->count();

        if ($picsNum >= 10) {
            abort(404, "Tele van a tárhely!");
        }

        $picture = Picture::create($request->all());

        Log::info("Kép hozzáadva: {$picture->name}");

        return response($picture);
    }

    public function filterImages(Request $request) {
        $orderBy = $request->get('orderBy', null);
        $category_id = $request->get('category', null);
        $artist_id = $request->get('artist_id', null);
        $orderDirection = $request->get('orderDirection', 'asc');
        $groupBy = $request->get('groupBy', null);
        $take = $request->get('take', null);
        $withRevisions = $request->get('withRevisions', null);
        $revisionsOfMentorId = $request->get('revisionsOfMentorId', null);
        $showRefusedImages = $request->get('showRefusedImages', null);
        $showFreshImages = $request->get('showFreshImages', null);
        $revisionOrder = $request->get('revisionOrder', null);
        //..

        $baseQuery = Picture::with('artist', 'category');

        if ($withRevisions) {

            if (!!$revisionsOfMentorId) {
                $baseQuery->with(
                    [
                        'revisions' => function ($q) use ($revisionsOfMentorId) {
                            $q->where('mentor_id', '=', $revisionsOfMentorId);

                            return $q;
                        },
                    ]
                );
            } else {
                $baseQuery->with('revisions');
            }
        }

        $conditions = function ($query) use ($category_id, $artist_id, $showRefusedImages) {
            if (!!$category_id) {
                $query->where('category_id', '=', $category_id);
            }
            if (!!$artist_id) {
                $query->where('artist_id', '=', $artist_id);
            }
            if (!$showRefusedImages) {
                $query->whereNull('refused_at');
            }
        };

        if (!!$orderBy) {
            $baseQuery->orderBy($orderBy, $orderDirection);
        } else {
            $baseQuery->inRandomOrder();
        }

        $baseQuery->where($conditions);

        if (!!$take && !$groupBy) {
            $datas = $baseQuery->take($take);
        } else {
            $datas = $baseQuery->get();
        }

        if (!!$revisionOrder) {
            $datas = $datas->map(
                function ($e) {
                    $e['avgScore'] = $e->revisions->avg('value');

                    return $e;
                }
            )->sortByDesc('avgScore');
        }

        if (!$showFreshImages) {
            $datas = $datas->filter(
                function ($value) {
                    $created = new Carbon($value['created_at']);
                    $created->addMinutes(3);

                    return Carbon::now()->greaterThanOrEqualTo($created);
                }
            );
            $datas = $datas->values();
        }

        if (!!$groupBy) {
            $col = $groupBy === "category" ? "category.name" : "artist.username";
            $datas = $datas->groupBy($col);
            if ($revisionOrder) {
                $datas = $datas->sortByDesc('avgScore');
            }

            if (!!$take) {
                $datas = $datas->map(
                    function ($e) use ($take) {
                        return $e->take($take);
                    }
                );
            }
        }

        return response($datas);
    }

    public function imageVisited($id) {
        $pic = Picture::findOrFail($id);
        $pic->visited++;

        if (!$pic->save()) {
            abort(500, "Nem sikerült a mentés");
        }

        return $pic->visited;
    }

    public function imageLiked($id) {
        $pic = Picture::findOrFail($id);
        $pic->liked++;

        if (!$pic->save()) {
            abort(500, "Nem sikerült a mentés");
        }

        return $pic->liked;
    }

    public function imageReviewed($id) {
        $pic = Picture::findOrFail($id);
        $pic->reviewed++;

        if (!$pic->save()) {
            abort(500, "Nem sikerült a mentés");
        }

        return $pic->reviewed;
    }

    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();
            $picture = Picture::findOrFail($id);
            $picture->fill($request->all());
            $picture->save();
            Log::info("{$picture->name} kép sikeresen módosítva.");
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A kép módosítása sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($picture);
    }

    public function show($id) {
        $picture = Picture::findOrFail($id);

        return response($picture);
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            $picture = Picture::findOrFail($id);
            $picture->delete();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("A kép törlése sikertelen!");
            Log::error($exception->getTraceAsString());
            throw $exception;
        }

        return response($picture);
    }
}
