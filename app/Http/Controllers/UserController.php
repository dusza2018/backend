<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller {
    public function index() {
        return User::get()->toArray();
    }

    public function getAllArtist() {
       return User::where('permission','=',1)->get()->toArray();
    }


    public function store(Request $request) {
        $user = new User();
        $user->fill($request->all());
        if (!$user->save()) {
            $this->response->error('could_not_create_user', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        Log::info("Added new user:" . json_encode($user));

        return $user->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = User::findOrFail($id);

        return $user->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        Log::info("updated:" . json_encode($user));
        $user->fill($request->all());
        if (!$user->save()) {
            $this->response->error('could_not_update_user', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $user->toArray();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Log::info("deleted user with id: " . $id);
        $user = User::findOrFail($id);
        if (!$user->delete()) {
            $this->response->error('could_not_delete_user', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $user->toArray();
    }

    public function changePassword(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();

        $password = $request->get('password');
        $password_conf = $request->get('password_confirmation');

        if ($password !== $password_conf) {
            $this->response->error('passwords dont match', 405);
        }

        if (strlen($password) < 6) {
            $this->response->error('too_short_password', 405);
        }

        $user->password = $password;
        if (!$user->save()) {
            $this->response->error('could_not_change_password', 500);
        }

        return response()->json(['status' => 'ok']);
    }
}
