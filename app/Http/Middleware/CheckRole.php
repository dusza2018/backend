<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole {


    public function handle($request, Closure $next, $role) {

        if ($request->user()['permission'] !== intval($role)) {
            abort(401);
        }

        return $next($request);
    }
}