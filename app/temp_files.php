<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class temp_files extends Model
{
    protected $fillable = ['uploader_id', 'file_path'];
}
