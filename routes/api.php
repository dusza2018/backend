<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'prefix'     => 'auth',
        'middleware' => 'api',
    ],
    function (Router $api) {
        $api->post('login', 'AuthController@login');
        $api->post('signup', 'AuthController@signUp');
    }
);

Route::group(
    ['middleware' => 'api'],
    function (Router $api) {
        $api->get('categories', CategoryController::class . '@index');
        $api->get('users/allartist', UserController::class . '@getAllArtist');
        $api->post('pictures/filterimages', PictureController::class . '@filterImages');
        $api->get('pictures/imageVisited/{id}', PictureController::class . '@imageVisited');
        $api->get('pictures/imageLiked/{id}', PictureController::class . '@imageLiked');
        $api->get('randomad', AdvertisementController::class . '@getRandom');
        $api->get('stepclicks/{id}', AdvertisementController::class . '@stepClicks');
        $api->get('test', PictureController::class . '@getScoreAvgValues');
    }
);

Route::group(
    ['middleware' => 'jwt.auth'],
    function (Router $api) {

        $api->post('files/add', files\FileController::class . '@store');
        $api->delete('files/{ids}', files\FileController::class . '@destroy');

        $api->apiResource(
            'categories',
            CategoryController::class,
            [
                'only' => ['store', 'update', 'destroy'],
            ]
        );

        $api->apiResource(
            'pictures',
            PictureController::class,
            [
                'only' => ['store', 'update', 'destroy', 'show'],
            ]
        );

        $api->apiResource(
            'ads', AdvertisementController::class,
            [
                'only' => ['store', 'update', 'destroy','index'],
            ]
        );

        $api->apiResource(
            'revisions',
            RevisionController::class,
            [
                'only' => ['store', 'update', 'destroy', 'show'],
            ]
        );

        $api->apiResource(
            'users',
            UserController::class
        );

        $api->group(
            ['middleware' => 'role:3'],
            function (Router $api) {
                $api->get('getmentorstats', RevisionController::class . '@getRevisionsByMentor');

            }
        );

        $api->group(
            ['middleware' => 'role:2'],
            function (Router $api) {
                $api->get('pictures/imageReviewed/{id}', PictureController::class . '@imageReviewed');
            }
        );

    }
);

